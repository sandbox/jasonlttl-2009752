<?php

/**
 * @file EXAMPLE.drush.inc
 * Provides a simple drush extension example
 */

function gaag_drush_command() {
  return array('gaag-up' =>
    array(
      'description' => 'Updates analytics for profiles',
      'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    ),
  );
}

function drush_gaag_up_validate() {
  // Return
  // drush_set_error('MACHINE_NAME_OF_FAIL', dt('Command failed because [reason]'));
  // on error.
}

function drush_gaag_up() {

  // Profiles
  $profiles = array(
    'ga:14003413' => 'ENGINEERING', 
    'ga:49242414' => 'BME', 
    'ga:49240558' => 'CBE', 
    'ga:26822073' => 'CEG', 
    'ga:33625035' => 'ECE',
    'ga:41260584' => 'MAE',
    'ga:49243116' => 'MSE',
    'ga:26822039' => 'EEIC',
    'ga:64413391' => 'CAREER:Symplicity',
    'ga:49242812' => 'CAREER',
    'ga:72291518' => 'STREETSEEN',
    );

  foreach ($profiles as $profile_id => $name) {
    print $name . "\n";
  	$data = gaag_get_profile_basic($profile_id);
  	foreach ($data['totalsForAllResults'] as $key => $val) {
      print "\t$key " . round($val, 1) . "\n";
    }
  }
}