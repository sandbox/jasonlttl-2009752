<?php

function gaag_service() {

  // create client object and set app name
  $client = new Google_Client();
  $client->setApplicationName('analytics'); // name of your app

  // set assertion credentials
  $client->setAssertionCredentials(
    new Google_AssertionCredentials(
      variable_get('gaag_service_email' , ''), 
      array('https://www.googleapis.com/auth/analytics.readonly'),
      variable_get('gaag_service_key' , '')
    ));

  // other settings
  $client->setClientId(variable_get('gaag_service_client_id' , ''));          
  $client->setAccessType('offline_access');

  // create service and get data
  $service = new Google_AnalyticsService($client);  

  return $service;
}

function gaag_load_library() {

  if (($library = libraries_detect('google-api-php-client')) && !empty($library['installed'])) {
    // echo 'The library is installed. Awesome!';
  }
  else {
    drupal_set_message($library['error message']);
  }

  return libraries_load('google-api-php-client');	

}


function gaag_get_profile_basic($profile_id, $dimensions='ga:year,ga:month') {
  
  // Verify and load the library
  gaag_load_library();
  $dimensions = '';
  $extra = array(
    'dimensions' => $dimensions,
    // 'sort' => '-ga:visits,ga:source',
    // 'filters' => 'ga:medium==organic',
    // 'max-results' => '25'
    );

  $service = gaag_service();

  $data = $service->data_ga->get(
    $profile_id, 
    '2012-07-01',
    '2013-07-01',
    ''
    // Visitors 
    . 'ga:visitors,ga:newVisits,' 
    . 'ga:percentNewVisits,'
    // Session
    . 'ga:visits,'
    . 'ga:entranceBounceRate,ga:visitBounceRate,ga:avgTimeOnSite,'
    // Page Tracking
    . 'ga:pageviews,ga:pageviewsPerVisit',
    $extra);

  return $data;
}